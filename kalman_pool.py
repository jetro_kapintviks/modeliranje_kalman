__author__ = 'Kiks'

import math
import pickle
import os
import numpy as np
import pandas as pd
from numpy.linalg import inv,pinv
import matplotlib.pyplot as plt
import sys
from IPython import display
import time
%matplotlib inline

__author__ = 'Kiks'

def triM(*args):
    startM=args[0]
    for m in args[1:]:
        startM=startM.dot(m)
    return startM

def getX_min(x_row,o_row,timeD):
    xn1=x_row[0]+o_row[1]*math.cos(x_row[2])*timeD
    yn1=x_row[1]+o_row[1]*math.sin(x_row[2])*timeD
    thet1=x_row[2]+o_row[2]*timeD
    return np.array([xn1,yn1,thet1])
def getHfun(curX,lanX):
    v=math.sqrt((curX[0]-lanX[0])**2+(curX[1]-lanX[1])**2)
    w=math.atan2(lanX[1]-curX[1],lanX[0]-curX[0])-curX[2]
    return np.array([v,w])

def getH(curX,lanX,Hfun):
    VpoX=(curX[0]-lanX[0])/(Hfun[0])
    VpoY=(curX[1]-lanX[1])/(Hfun[0])
    denWder=( (lanX[0]-curX[0])**2 +(lanX[1]-curX[1])**2 )

    WpoX=(lanX[1]-curX[1])/denWder
    WpoY=-(lanX[0]-curX[0])/denWder

    WpoTh=-1
    return np.array([ [VpoX,VpoY,0],[WpoX,WpoY,-1] ])

def getA(x_row,o_row,timeD):
    a=np.eye(3,3)
    a[0,2]=-o_row[1]*math.sin(x_row[2])*timeD
    a[1,2]=o_row[1]*math.cos(x_row[2])*timeD
    return a
def getP_min(AK,PK_1,QK):
    return triM(AK,PK_1,AK.T) + \
    (QK)

def getK(PK_,HK,RK):
    return triM(PK_,HK.T,
           inv(  triM(HK,PK_,HK.T)
                   +RK))

def getX_post(x_row,KK,z_row,h_row):
    return x_row+triM(KK,(z_row-h_row))

def getP(I,KK,HK,PK_):
    return  (I- np.dot(KK,HK)).dot(PK_)
def absdist(x1,x2):
    return abs(x1[0]-x2[0])+abs(x1[1]-x2[1])

def getbaseX(groun,odom):
    if len(odom)<1:
        print('no ground baseX')
        sys.exit(-5)
    fin=np.where(groun[:,0]<=odom[0,0])[0]
    if len(fin)<1:
        print("no baseX for first odom")
        sys.exit(-5)
        #return getbaseX(groun,odom[1:])
    return fin[-1]
def euc_dist(a,b):
    return math.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2 )


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++


i=3

oName='datasets/Robot{}_Odometry.dat'.format(i)
mName='datasets/Robot{}_Measurement.dat'.format(i)
gName='datasets/Robot{}_Groundtruth.dat'.format(i)
lName='datasets/Landmark_Groundtruth.dat'
bName='datasets/Barcodes.dat'

namesOdom="Time[s]    forward_velocity[m/s]    angular_velocity[rad/s]".split()
odom=pd.read_csv(oName,sep=r"\s*",skiprows=4,names=namesOdom,engine='python')

namesMeas="Time[s]    Subject_#    range[m]    bearing[rad]".split()
meas=pd.read_csv(mName,sep=r"\s*",skiprows=4,names=namesMeas,engine='python')

namesGroun="Time[s]    x[m]    y[m]    orientation[rad]".split()
groun=pd.read_csv(gName,sep=r"\s*",skiprows=4,names=namesGroun,engine='python')

namesLand=" Subject#    x[m]    y[m]    xstd-dev[m]    ystd-dev[m]".split()
land=pd.read_csv(lName,sep=r"\s*",skiprows=4,names=namesLand,engine='python')

namesBar=" Subject#    Barcode#".split()
bar=pd.read_csv(bName,sep=r"\s*",skiprows=4,names=namesBar,engine='python')

odom=np.array(odom.values)
meas=np.array(meas.values)
groun=np.array(groun.values)
land=np.array(land.values)
bar=np.array(bar.values)

pool=np.zeros((len(odom)+len(meas),5))



for ind,o in enumerate(odom):
    pool[ind,0]=1
    pool[ind,1:4]=o

sInd=ind+1
for ind,m in enumerate(meas):
    pool[sInd+ind,0]=2
    pool[sInd+ind,1:5]=m

#sInd+=ind+1
#for ind,g in enumerate(groun):
#    pool[sInd+ind,1:5]=g


print(len(pool),sInd+ind+1,pool[:5])
np.set_printoptions(precision=12)
#print(sInd+ind+1,len(pool),len(meas),len(odom),len(groun),
#      pool[:5],pool[97347:97354],
#      pool[97351+8742-2:97351+8742+2],pool[-5:],sep='\n')
pool[pool[:,1].argsort(),:20]
def  getGrounForOdom(groun,odom):
    return [np.where(groun[:,0]>=o[0])[0][0] for o in odom]

def processKal(pool,groun):
   xCalc=np.zeros((odom.shape))
   xTime=np.zeros((odom.shape[0],))
   xPost=np.zeros((odom.shape[0],),dtype=bool)
   for o in odom:
        timeD=row[1]-curTime
        xCalc[xInd,1:]=getX_min(xCalc[xInd-1],row[1:],timeD)
        xTime[xInd]=row[1]

        curA=getA(xCalc[xInd,1:],row[1:],timeD)
        PMat=getP_min(curA,PMat,QMat)
            curTime=row[0]

processKal(pool,groun)