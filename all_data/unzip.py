
import os,zipfile


zips=[i for i in os.listdir('.') if i.endswith('.zip')]

for file in zips:
	with zipfile.ZipFile(file) as zf:
		zf.extractall()