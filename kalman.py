import math
import pickle
import numpy as np
import pandas as pd
from numpy.linalg import inv
import matplotlib.pyplot as plt
import sys

__author__ = 'Kiks'

def triM(*args):
    startM=args[0]
    for m in args[1:]:
        startM=startM.dot(m)
    return startM

def getX_min(x_row,o_row,timeD):
    xn1=x_row[0]+o_row[1]*math.cos(x_row[2])*timeD
    yn1=x_row[1]+o_row[1]*math.sin(x_row[2])*timeD
    thet1=x_row[2]+o_row[2]*timeD
    return np.array([xn1,yn1,thet1])


def getA(x_row,o_row,timeD):
    a=np.eye(3,3)
    a[0,2]=-o_row[1]*math.sin(x_row[2])*timeD
    a[1,2]=o_row[1]*math.cos(x_row[2])*timeD
    return a
def getP_min(AK,PK_1,QK):
    return triM(AK,PK_1,AK.T) + \
    (QK)

def getK(PK_,HK,RK):
    return triM(PK_,HK.T,
           inv(  triM(HK,PK_,HK.T)
                   +RK))

def getX_post(x_row,KK,z_row):
    return x_row+triM(KK,(z_row-x_row))

def getP(I,KK,HK,PK_):
    return  (I- np.dot(KK,HK)).dot(PK_)
def absdist(x1,x2):
    return abs(x1[0]-x2[0])+abs(x1[1]-x2[1])

def process(moe,groun,odom):
    fInd=np.where(moe>0)[0][0]
    xCalc=np.zeros((odom.shape[0]+1,odom.shape[1]),dtype=float)
    xCalc[0]=groun[fInd-1,1:]
    startInd=0

    # PMat=np.zeros((odom.shape[0]+1,odom.shape[1]),dtype=float)
    PMat=np.zeros((3,3),dtype=float)

    QMat=np.ones((3,3),dtype=float)*1e-5
    RMat=np.zeros((3,3))
    HMat=np.eye(3)
    IMat=np.eye(3,3)

    postEd=np.zeros((odom.shape[0]+1,),dtype=int)
    postEd[0]=1
    curTime=groun[fInd-1,0]
    for i,mind in enumerate(np.array(moe[fInd:],dtype=int)):



        for insInd in range(startInd,startInd+mind):
            timeD=odom[insInd,0]-curTime
            xCalc[insInd+1]=getX_min(xCalc[insInd],odom[insInd],timeD)
            curA=getA(xCalc[insInd],odom[insInd],timeD)
            PMat=getP_min(curA,PMat,QMat)

            curTime=odom[insInd,0]
        startInd+=mind
        postEd[startInd]=1
        if mind!=0:
            try:
                KMat=getK(PMat,HMat,RMat)
            except:
                print(i)
                sys.exit(-10)
            xCalc[startInd]=getX_post(xCalc[startInd],KMat,groun[fInd+i][1:])
            PMat=getP(IMat,KMat,HMat,PMat)

    howMuch=sum(postEd==1)
    upper=6
    lower=-4


    pxCalc=xCalc[np.where(postEd==1)]
    # pxCalc=pxCalc[  (pxCalc[:,0]<upper) & (pxCalc[:,0]>lower) &
    #                 (pxCalc[:,1]<upper) & (pxCalc[:,1]>lower)]
    p1xCalc=np.clip(pxCalc,lower,upper)
    pgroun=groun[ np.where(moe>0) ]
    print(len(pgroun),len(p1xCalc))

    pmoe=np.array(moe[fInd:])
    pmoe=pmoe[pmoe>0]

    p2xCalc=[abs(a[0]-b[0])+abs(a[1]-b[1]) for a,b in zip(p1xCalc[:-1],p1xCalc[1:])  ]
    p3xCalc=np.zeros(p1xCalc.shape)
    old=p1xCalc[0]
    # p2xCalc.insert(0,0)
    for ind,(x1,x2) in enumerate(zip(p1xCalc,p1xCalc[1:])):
        if absdist(x1,old)>1:
            p3xCalc[ind]=old
        else:
            old=x1
            sind=ind
            p3xCalc[ind]=x1
    print (len(pmoe),len(p2xCalc))
    print(sum(np.array([absdist(a,(b[1],b[2]))
                        for a,b in zip(p1xCalc[1:],pgroun)])<0.05)/len(pgroun))


    plt.subplots_adjust(top=0.85)
    plt.subplot(212)
    plt.plot(p1xCalc[:howMuch,0],p1xCalc[:howMuch,1],'r-')
    plt.plot(pgroun[:howMuch,1],pgroun[:howMuch,2],'g-')

    plt.subplot(211)
    plt.title("Comparison of groundtruth and odometry-\n corrected by "
              +"groundtruth(groundtruth in green)\nFirst {0}/{1} examples shown".format(howMuch,len(pxCalc)))

    plt.plot(pgroun[:howMuch,1],pgroun[:howMuch,2],'g-')
    plt.plot(p1xCalc[:howMuch,0],p1xCalc[:howMuch,1],'r-')

    plt.savefig('comparison3.png',dpi=210)

    plt.figure()
    plt.title("Comparison of spikes in odometry vs.\n number of odometries per 1 groundtruth")
    # plt.subplot(211)
    plt.plot(pmoe,'g-')

    # plt.subplot(212)
    plt.plot(p2xCalc,'r-')
    plt.savefig("spikes_gaps.png",dpi=210)


    plt.figure()
    plt.title("Absolute Difference between groundtruth\n and odometry-corrected")
    plt.plot([absdist(a,(b[1],b[2])) for a,b in zip(p1xCalc[1:],pgroun)])

    # plt.plot([absdist(a,(b[1],b[2]))
    #           if absdist(a,(b[1],b[2] ) )<20 else -1
    #           for i,a,b in zip(range(len(pgroun))
    #                                                   ,pxCalc[1:],pgroun) ])
    plt.savefig("diff_ground_odometry2.png",dpi=210)
    plt.show()




oName='datasets/Robot1_Odometry.dat'
mName='datasets/Robot1_Measurement.dat'
gName='datasets/Robot1_Groundtruth.dat'


namesOdom="Time[s]    forward_velocity[m/s]    angular_velocity[rad/s]".split()
odom=pd.read_csv(oName,sep=r"\s*",skiprows=4,names=namesOdom,engine='python')

namesMeas="Time[s]    Subject_#    range[m]    bearing[rad]".split()
meas=pd.read_csv(mName,sep=r"\s*",skiprows=4,names=namesMeas,engine='python')

namesGroun="Time[s]    x[m]    y[m]    orientation[rad]".split()
groun=pd.read_csv(gName,sep=r"\s*",skiprows=4,names=namesGroun,engine='python')

odom=np.array(odom.values)
meas=np.array(meas.values)
groun=np.array(groun.values)

with open("between.rows","rb") as f:
    moe=pickle.load(f)

process(moe,groun,odom)