import pickle
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# f=open()
# for red in f:
	# matRed=red.split()
oName='datasets/Robot1_Odometry.dat'
mName='datasets/Robot1_Measurement.dat'
gName='datasets/Robot1_Groundtruth.dat'

# oName='../odom.dat'
# mName='../meas.dat'

namesOdom="Time[s]    forward_velocity[m/s]    angular_velocity[rad/s]".split()
odom=pd.read_csv(oName,sep=r"\s*",skiprows=4,names=namesOdom)

namesMeas="Time[s]    Subject_#    range[m]    bearing[rad]".split()
meas=pd.read_csv(mName,sep=r"\s*",skiprows=4,names=namesMeas)

namesGroun="Time[s]    x[m]    y[m]    orientation[rad]".split()
groun=pd.read_csv(gName,sep=r"\s*",skiprows=4,names=namesGroun)

print(groun.head())
print(len(set(groun.iloc[:,0])))
print(len(groun.iloc[:,0]))
# print(odom.head(),len(odom))
meas=groun
print(meas.iloc[0,0])
i=0
j=0
N=len(odom)
M=len(meas)
eden=[-1]
dva=[]
ednak=0
state=0
while i<N and j<M:
	if i%200==2: print (i,flush=True)

	if meas.iloc[j,0]>odom.iloc[i,0]:
		eden[-1]+=1
		state=0
		i+=1
	elif meas.iloc[j,0]<=odom.iloc[i,0]:
		j+=1
		state=0
		if j<M and  meas.iloc[j,0]<odom.iloc[i,0]:
			eden.append(i-1)
		elif j<M and  meas.iloc[j,0]==odom.iloc[i,0]:
			eden.append(i)
		elif j<M:
			eden.append(i-1)

	# elif state==0 and meas.iloc[j,0]==odom.iloc[i,0]:
	# 	i+=1
	# 	state=1
	# elif state==1 and  meas.iloc[j,0]==odom.iloc[i,0]:
	# 	j+=1
moe=np.zeros(M,dtype=float)
moe[0]=eden[0]
for i,x in enumerate(eden[1:]):
	moe[i+1]=(eden[i+1]-eden[i])
print(eden,dva,ednak)
plt.subplot(211)
plt.plot(moe)
plt.subplot(212)
plt.plot(moe[moe<50])
# plt.show()
plt.savefig('figura_groun.png',dpi=140)
np.savetxt("kolku_broj_groun.txt",np.array(moe,dtype=int))

with open("between.rows","wb") as f:
	pickle.dump(moe,f)


# f.close()